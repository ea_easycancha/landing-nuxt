import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _2519c1db = () => interopDefault(import('../pages/landing.vue' /* webpackChunkName: "pages/landing" */))
const _ef1cd00a = () => interopDefault(import('../pages/albums/_id.vue' /* webpackChunkName: "pages/albums/_id" */))
const _4d031616 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/landing",
    component: _2519c1db,
    name: "landing"
  }, {
    path: "/albums/:id?",
    component: _ef1cd00a,
    name: "albums-id"
  }, {
    path: "/",
    component: _4d031616,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
