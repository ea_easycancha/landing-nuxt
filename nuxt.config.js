export default {
	css: ["bulma/css/bulma.css", "~/assets/style.css"],
	head: {
		meta: [
			{ charset: "utf-8" },
			{ name: "viewport", content: "width=device-width, initial-scale=1" }
		],
		link: [
			{
				rel: "stylesheet",
				href: "https://fonts.googleapis.com/css?family=Montserrat:300,400,700&display=swap"
			}
		]
	}
};
